<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

/**
 * Class SlackWebhookValidator
 *
 * Validate Slack incoming webhook format.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
class SlackWebhookValidator implements SlackWebhookValidatorInterface
{
    private const DEFAULT_WEBHOOK_REGEX_VALIDATOR = '/^https:\/\/hooks\.slack\.com\/services\/T[a-zA-Z0-9]{6,}\/B[a-zA-Z0-9]{6,}\/[a-zA-Z0-9]{6,}$/m';

    /**
     * @var string
     */
    private $webhookRegexValidator;

    /**
     * SlackWebhookValidator constructor.
     *
     * @param string|null $webhookRegexValidator
     */
    public function __construct(
        string $webhookRegexValidator = self::DEFAULT_WEBHOOK_REGEX_VALIDATOR
    ) {
        $this->webhookRegexValidator = $webhookRegexValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(string $webhookUrl): void
    {
        if (!$this->isValid($webhookUrl)) {
            throw new \DomainException(
                \sprintf(
                    'Webhook url has an invalid value, \'%s\' was given.',
                    $webhookUrl
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(string $webhookUrl): bool
    {
        return (bool)\preg_match($this->webhookRegexValidator, \trim($webhookUrl));
    }
}
