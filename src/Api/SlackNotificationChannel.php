<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelTrait;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationLevelInterface;
use Interactiv4\Contracts\SPL\Notification\Api\NotificationMessageInterface;

/**
 * Class SlackNotificationChannel
 *
 * Handle outgoing requests for notification messages to Slack channels through incoming webhook.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
class SlackNotificationChannel implements SlackNotificationChannelInterface
{
    use NotificationChannelTrait;

    /**
     * Slack incoming webhook endpoint.
     *
     * @var string
     */
    private $webhookUrl;

    /**
     * SlackNotificationChannel constructor.
     *
     * @param NotificationLevelInterface $notificationLevel
     * @param int                        $notificationLevelMask
     * @param string                     $webhookUrl
     */
    public function __construct(
        NotificationLevelInterface $notificationLevel,
        int $notificationLevelMask,
        string $webhookUrl
    ) {
        $this->notificationLevel = $notificationLevel;
        $this->notificationLevelMask = $notificationLevelMask;
        $this->webhookUrl = $webhookUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function send(NotificationMessageInterface $message): void
    {
        try {
            $slackNotifier = new SlackNotifier;

            $slackClientOptions = \array_replace_recursive(
                $message->getAdditionalData()[SlackNotifierInterface::ADDITIONAL_DATA_NAMESPACE] ?? [],
                [
                    SlackNotifierInterface::ADDITIONAL_DATA_KEY_WEBHOOK_URL => $this->webhookUrl,
                ]
            );

            $slackNotifier->notify(
                $message->getLevel(),
                $message->getTitle(),
                $message->getBody(),
                [
                    SlackNotifierInterface::ADDITIONAL_DATA_NAMESPACE => $slackClientOptions
                ]
            );
        } catch (CouldNotSendNotificationMessageException $e) {
            throw $e;
        } catch (\Throwable $t) {
            throw new CouldNotSendNotificationMessageException(
                \sprintf(
                    'Some error(s) occurred while sending Slack notification: %s',
                    $t->getMessage()
                )
            );
        }
    }
}
