<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

/**
 * Class SlackWebhookValidator
 *
 * Validate Slack incoming webhook format.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
interface SlackWebhookValidatorInterface
{
    /**
     * Validate Slack webhook url format.
     *
     * @param string $webhookUrl
     *
     * @return void
     *
     * @throws \DomainException
     */
    public function validate(string $webhookUrl): void;

    /**
     * Check if Slack webhook url format is valid.
     *
     * @param string $webhookUrl
     *
     * @return bool
     */
    public function isValid(string $webhookUrl): bool;
}
