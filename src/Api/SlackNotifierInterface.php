<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotifierInterface;

/**
 * Interface SlackNotifierInterface
 *
 * Notify a message to Slack using an incoming webhook.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
interface SlackNotifierInterface extends NotifierInterface
{
    /**
     * Reserved namespace within additional data for additional info regarding only Slack messages.
     */
    public const ADDITIONAL_DATA_NAMESPACE = 'Slack';

    /**
     * Additional data keys for Slack client configuration.
     */
    public const ADDITIONAL_DATA_KEY_WEBHOOK_URL = 'webhook_url';

    public const ADDITIONAL_DATA_KEY_LINK_NAMES = 'link_names';

    public const ADDITIONAL_DATA_KEY_UNFURL_LINKS = 'unfurl_links';

    public const ADDITIONAL_DATA_KEY_UNFURL_MEDIA = 'unfurl_media';

    public const ADDITIONAL_DATA_KEY_ALLOW_MARKDOWN = 'allow_markdown';
}
