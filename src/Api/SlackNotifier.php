<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

use Interactiv4\Contracts\SPL\Notification\Api\Exception\CouldNotSendNotificationMessageException;
use Maknz\Slack\Client as SlackClient;

/**
 * Class SlackNotifier
 *
 * Notify a message to Slack using an incoming webhook.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
class SlackNotifier implements SlackNotifierInterface
{
    /**
     * @var SlackWebhookValidator
     */
    private $webhookValidator;

    /**
     * SlackNotifier constructor.
     */
    public function __construct()
    {
        $this->webhookValidator = new SlackWebhookValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function notify(
        int $level,
        ?string $title = null,
        ?string $body = null,
        array $additionalData = []
    ): void {
        try {
            $slackClientOptions = $this->filterSlackClientOptions(
                \array_replace_recursive(
                    [
                        self::ADDITIONAL_DATA_KEY_WEBHOOK_URL    => null,
                        self::ADDITIONAL_DATA_KEY_LINK_NAMES     => true,
                        self::ADDITIONAL_DATA_KEY_UNFURL_LINKS   => true,
                        self::ADDITIONAL_DATA_KEY_UNFURL_MEDIA   => true,
                        self::ADDITIONAL_DATA_KEY_ALLOW_MARKDOWN => true,
                    ],
                    $additionalData[SlackNotifierInterface::ADDITIONAL_DATA_NAMESPACE] ?? []
                )
            );

            $slackClient = new SlackClient(
                $this->extractWebhookUrl($slackClientOptions), $slackClientOptions
            );

            $slackClient->createMessage()->send($this->getMessage($title, $body));
        } catch (CouldNotSendNotificationMessageException $e) {
            throw $e;
        } catch (\Throwable $t) {
            throw new CouldNotSendNotificationMessageException(
                \sprintf(
                    'Some error(s) occurred while sending Slack notification: %s',
                    $t->getMessage()
                )
            );
        }
    }

    /**
     * Extract webhook url from Slack client options.
     * Return value and unset from options.
     *
     * @param array $slackClientOptions
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    private function extractWebhookUrl(array &$slackClientOptions): string
    {
        $webhookUrl = $slackClientOptions[self::ADDITIONAL_DATA_KEY_WEBHOOK_URL] ?? null;

        if (null === $webhookUrl) {
            throw new \InvalidArgumentException(
                \sprintf(
                    'Missing required argument %s in additional data namespace %s',
                    self::ADDITIONAL_DATA_KEY_WEBHOOK_URL,
                    self::ADDITIONAL_DATA_NAMESPACE
                )
            );
        }

        $this->webhookValidator->validate($webhookUrl);

        unset($slackClientOptions[self::ADDITIONAL_DATA_KEY_WEBHOOK_URL]);

        return \trim($webhookUrl);
    }

    /**
     * Get message parts.
     *
     * @param string|null $title
     * @param string|null $body
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    private function getMessage(
        ?string $title = null,
        ?string $body = null
    ): string {
        $messageParts = \array_filter(
            [
                $title,
                $body,
            ],
            static function ($messagePart): bool {
                return $messagePart !== null && 0 < \strlen((string)$messagePart);
            }
        );

        if (!\count($messageParts)) {
            throw new \InvalidArgumentException('Title and body message cannot be both empty');
        }

        return \implode(PHP_EOL, $messageParts);
    }

    /**
     * You cannot override the default channel (chosen by the user who installed your app),
     * username, or icon when you're using Incoming Webhooks to post messages.
     * Instead, these values will always inherit from the associated Slack app configuration.
     *
     * @see https://api.slack.com/messaging/webhooks#advanced_message_formatting
     *
     * @param array $slackClientOptions
     *
     * @return array
     */
    private function filterSlackClientOptions(array $slackClientOptions): array
    {
        unset($slackClientOptions['channel'], $slackClientOptions['username'], $slackClientOptions['icon']);

        return $slackClientOptions;
    }
}
