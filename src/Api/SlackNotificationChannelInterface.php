<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\SPL\Notification\Slack\Api;

use Interactiv4\Contracts\SPL\Notification\Api\NotificationChannelInterface;

/**
 * Interface SlackNotificationChannelInterface
 *
 * Handle outgoing requests for notification messages to Slack channels through incoming webhook.
 *
 * @api
 *
 * @package Interactiv4\SPL\Notification\Slack
 */
interface SlackNotificationChannelInterface extends NotificationChannelInterface
{
}
